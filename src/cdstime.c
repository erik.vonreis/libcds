static char* versionId = "Version $Id$";
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: tconv							*/
/*                                                         		*/
/* Procedure Description: prvides functions to convert TAI and UTC;	*/
/* and functions to handle TAI (international atomic time)		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#include <string.h>
#include <time.h>
#include <stdio.h>

#include <glib-2.0/glib.h>

#include "cds/cdstime.h"

/* define some time constants */

#define SECS_PER_HOUR ( 60ul * 60ul )
#define SECS_PER_DAY ( SECS_PER_HOUR * 24 )
#define DAYS_PER_YEAR 365
#define NSECS_PER_SEC 1000000000lu

/**
 * Use NTP because IERS uses ntp time stamps in their leapseconds file.
 *
 * NTP starts 1900 Jan 1 00:00:00, A Monday
 *
 * NTP is effected by leapseconds, so an NTP second
 * at a positive leapsecond is two TAI seconds long.
 */
#define NTP_EPOCH_YEAR 1900
#define NTP_EPOCH_WEEKDAY 1

/* define leap year macros */
#define ISLEAP( year )                                                         \
    ( ( year ) % 4 == 0 && ( ( year ) % 100 != 0 || ( year ) % 400 == 0 ) )
#define LEAPS_THRU_END_OF( y ) ( ( y ) / 4 - ( y ) / 100 + ( y ) / 400 )
#define LEAPS_UNTIL( y ) LEAPS_THRU_END_OF( ( y - 1 ) )

/**
 * Calculate ntp time for a given year on Midnight Jan 1
 */
#define NTP_TIME_JAN1( y )                                                     \
    ( ( ( (y)-NTP_EPOCH_YEAR ) * DAYS_PER_YEAR +                               \
        ( LEAPS_UNTIL( y ) - LEAPS_UNTIL( NTP_EPOCH_YEAR ) ) ) *               \
      SECS_PER_DAY )

/**
 * Calculate the time for the given year on Midnight Jul 1
 */
#define NTP_TIME_JUL1( y )                                                     \
    ( ( ( (y)-NTP_EPOCH_YEAR ) * DAYS_PER_YEAR +                               \
        ( LEAPS_THRU_END_OF( y ) - LEAPS_UNTIL( NTP_EPOCH_YEAR ) ) + 181 ) *   \
      SECS_PER_DAY )

/**
 * Start of atomic time in terms of seconds from NTP epoch
 * At this point, UTC and GPC were synchronized
 * Jan 1, 1958
 */
#define TAI_EPOCH NTP_TIME_JAN1( 1958 )

/**
 * GPS time starts on Jan 6, 1980
 * in terms of seconds from NTP epoch
 */
#define GPS_EPOCH ( NTP_TIME_JAN1( 1980 ) + 5 * SECS_PER_DAY )

/**
 * Needed to convert unix timestamps
 */
#define UNIX_EPOCH NTP_TIME_JAN1( 1970 )

typedef int64_t ntp_s_t;

/* How many days come before each month (0-12) */
static const unsigned short int mon_yday[ 2 ][ 13 ] = {
    /* Normal years.  */
    { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 },
    /* Leap years.  */
    { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 }
};

/** Denotes a struct representing UTC leap seconds.

 @author DS, April 98
 @see Time Conversion
************************************************************************/
typedef struct leap_t
{
    /** TAI when the leap seconds takes effect. */
    ntp_s_t transition;
    /** Seconds of correction to apply. The correction is given as
     the new total difference between TAI and UTC. */
    int change;
} leap_t;

/**
 * Record of all UTC leapseconds
 */
typedef struct leap_table_t
{
    GArray* leap_seconds; /* of type leap_t */
    ntp_s_t expiration;
} leap_table_t;

leap_table_t*
new_leaptable( )
{
    return (leap_table_t*)malloc( sizeof( leap_table_t ) );
}

void
free_leaptable( leap_table_t* table )
{
    if ( NULL == table )
    {
        if ( table->leap_seconds != NULL )
        {
            g_array_free( table->leap_seconds, 1 );
        }
        free( table );
    }
}

/** After a call to a time conversion between time scales with leapseconds and without,
 * this should point to the array of leap seconds currently used in the conversions.
 */
static leap_table_t* leaps = NULL;

/* This table contains every NTP second which is a leap second
   the second argument is the correction to apply when changing from NTP to TAI.

   The 10sec difference at Jan. 1, 1972 represents the difference
   between UTC and TAI time when UTC was locked to the SI second.
   This difference developed between 1958 and 1972 in a way not defined by the table.
   They are not real leapseconds.
   Therefore, dates before Jan 1, 1972 cannot be accurately converted between TAI and UTC
   using the table.


   */

static ntp_s_t fallback_expiration =
    NTP_TIME_JAN1( 2024 ) - ( 10 * SECS_PER_DAY );
static size_t num_fallback_leaps = 28;
static leap_t fallback_leaps[] = {
    { NTP_TIME_JAN1( 1972 ),
      +10 }, /* Jan 1, 1972: UTC synchronized with TAI - ten seconds. */
    { NTP_TIME_JUL1( 1972 ), +11 }, /* Jul. 1, 1972 */
    { NTP_TIME_JAN1( 1973 ), +12 }, /* Jan. 1, 1973 */
    { NTP_TIME_JAN1( 1974 ), +13 }, /* Jan. 1, 1974 */
    { NTP_TIME_JAN1( 1975 ), +14 }, /* Jan. 1, 1975 */
    { NTP_TIME_JAN1( 1976 ), +15 }, /* Jan. 1, 1976 */
    { NTP_TIME_JAN1( 1977 ), +16 }, /* Jan. 1, 1977 */
    { NTP_TIME_JAN1( 1978 ), +17 }, /* Jan. 1, 1978 */
    { NTP_TIME_JAN1( 1979 ), +18 }, /* Jan. 1, 1979 */
    { NTP_TIME_JAN1( 1980 ), +19 }, /* Jan. 1, 1980 */
    { NTP_TIME_JUL1( 1981 ), +20 }, /* Jul. 1, 1981 */
    { NTP_TIME_JUL1( 1982 ), +21 }, /* Jul. 1, 1982 */
    { NTP_TIME_JUL1( 1983 ), +22 }, /* Jul. 1, 1983 */
    { NTP_TIME_JUL1( 1985 ), +23 }, /* Jul. 1, 1985 */
    { NTP_TIME_JAN1( 1988 ), +24 }, /* Jan. 1, 1988 */
    { NTP_TIME_JAN1( 1990 ), +25 }, /* Jan. 1, 1990 */
    { NTP_TIME_JAN1( 1991 ), +26 }, /* Jan. 1, 1991 */
    { NTP_TIME_JUL1( 1992 ), +27 }, /* Jul. 1, 1992 */
    { NTP_TIME_JUL1( 1993 ), +28 }, /* Jul. 1, 1993 */
    { NTP_TIME_JUL1( 1994 ), +29 }, /* Jul. 1, 1994 */
    { NTP_TIME_JAN1( 1996 ), +30 }, /* Jan. 1, 1996 */
    { NTP_TIME_JUL1( 1997 ), +31 }, /* Jul. 1, 1997 */
    { NTP_TIME_JAN1( 1999 ), +32 }, /* Jan. 1, 1999 */
    { NTP_TIME_JAN1( 2006 ), +33 }, /* Jan. 1, 2006 */
    { NTP_TIME_JAN1( 2009 ), +34 }, /* Jan. 1, 2009 */
    { NTP_TIME_JUL1( 2012 ), +35 }, /* Jul. 1, 2012 */
    { NTP_TIME_JUL1( 2015 ), +36 }, /* Jul. 1, 2015 */
    { NTP_TIME_JAN1( 2017 ), +37 }
}; /* Jan. 1, 2017 */

/**
     * Convert UTC date structure to NTP seconds,
     * that is, seconds since Jan 1, 1900.
     *
     * Since utc_t has only 1 second resoultion, we return ntp_s_t
     * type rather than fill in a structure.
     * @return seconds since Jan 1, 1900
     */
static ntp_s_t
NTPfromUTC_s( const utc_t* utc_ptr )
{
    int     year = utc_ptr->tm_year + 1900;
    ntp_s_t ntp_s = NTP_TIME_JAN1( year );
    ntp_s += utc_ptr->tm_yday * SECS_PER_DAY;
    ntp_s += utc_ptr->tm_hour * SECS_PER_HOUR;
    ntp_s += utc_ptr->tm_min * 60;
    ntp_s += utc_ptr->tm_sec;

    return ntp_s;
}

/**
 * Returns a leap table populated with the leaps from fallback_leaps.
 * the table is allocated by the function and it's pointer assigned to *table.
 * If the either the table or its GArray cannot be allocated, then 0 is returned and *table is left unchanged.
 * The caller is responsible for freeing the table.
 * @param table
 * @return
 */
static leap_table_t*
get_fallback_leaps( )
{
    leap_table_t* new_table = new_leaptable( );
    if ( NULL == new_table )
    {
        return NULL;
    }
    new_table->leap_seconds =
        g_array_sized_new( 0, 0, sizeof( leap_t ), num_fallback_leaps );
    if ( NULL == new_table->leap_seconds )
    {
        free_leaptable( new_table );
        return NULL;
    }
    memcpy( new_table->leap_seconds->data,
            fallback_leaps,
            sizeof( leap_t ) * num_fallback_leaps );
    new_table->leap_seconds->len = num_fallback_leaps;
    new_table->expiration = fallback_expiration;
    return new_table;
}

/**
 * Read the IERS leap second file
 *
 * This file lists NTP time versus total leap-seconds.
 * @return if successful loaded and the file is not expired
 * return 1, and *table will point to a new leap_table_t.
 *
 * If the file can't be opened or read, returns 0, and values
 * is unchanged.
 */
static leap_table_t*
read_IERS_leap_seconds_linux( )
{
    const size_t numnames = 3;
    const char*  fnames[] = {
         "/usr/share/zoneinfo/leap-seconds.list",
         "/usr/local/share/zoneinfo/leap-seconds.list",
         "/share/zoneinfo/leap-seconds.list",
    };
    char line[ 256 ];
    int  ret = 1;

    for ( size_t i = 0; i < numnames; i++ )
    {
        const char* fname = fnames[ i ];
        FILE*       f = fopen( fname, "rt" );
        if ( NULL == f )
        {
            continue;
        }
        leap_table_t* new_table = new_leaptable( );
        if ( NULL == new_table )
        {
            return NULL;
        }
        new_table->leap_seconds = g_array_new( 0, 0, sizeof( leap_t ) );
        if ( NULL == new_table->leap_seconds )
        {
            free_leaptable( new_table );
            return NULL;
        }

        while ( !feof( f ) )
        {
            fgets( line, sizeof( line ), f );
            if ( line[ 0 ] == '#' )
            {
                if ( line[ 1 ] == '@' )
                {
                    uint64_t expire_ntp = 0;
                    int num_matched = sscanf( line + 2, "%lu", &expire_ntp );
                    if ( num_matched == 0 )
                    {
                        fprintf( stderr,
                                 "failed to get expiration time from %s\n",
                                 fname );
                        expire_ntp = 0;
                    }

                    new_table->expiration = expire_ntp;
                }
            }
            if ( g_ascii_isdigit( line[ 0 ] ) )
            {
                leap_t new_leap;
                int    num_matched = sscanf(
                    line, "%lu %d", &new_leap.transition, &new_leap.change );
                if ( 2 == num_matched )
                {
                    g_array_append_val( new_table->leap_seconds, new_leap );
                }
                else
                {
                    fprintf( stderr,
                             "error reading table entry from file %s\n",
                             fname );
                }
            }
        }

        fclose( f );
        return new_table;
    }
    return NULL;
}

/**
 * Read the NIST leap second file
 *
 * The file lists the UTC times for leap seconds.  It does not include the +10
 * seconds TAI had over UTC on Jan 1, 1972.
 *
 * Format is
 * Leap   2012   Jun   30    23:59:60   +     S
 * For an added leap second.
 *
 * A dropped leap second would have a '-' instead of a plus, but this hasn't
 * happened.
 *
 * the leap_table_t pointer pointed to by 'table' will get an allocated leap_table_t structure.
 * The calling function is the owner of this new structure.
 *
 * Note that leap_t is in ntp times.
 *
 * @param table
 * @return 1 if leap_table_t structure is created and returned.
 * 0 if there's a file failure or allocation failure.
 */
leap_table_t*
read_NIST_leap_seconds_linux( )
{
    const size_t numnames = 3;
    const char*  fnames[] = {
         "/usr/share/zoneinfo/leapseconds",
         "/usr/local/share/zoneinfo/leapseconds",
         "/share/zoneinfo/leapseconds",
    };
    char line[ 256 ];
    int  ret = 1;
    for ( size_t i = 0; i < numnames; i++ )
    {
        const char* fname = fnames[ i ];
        FILE*       f = fopen( fname, "rt" );
        if ( NULL == f )
        {
            continue;
        }
        leap_table_t* new_table = new_leaptable( );
        if ( NULL == new_table )
        {
            return NULL;
        }
        new_table->leap_seconds = g_array_new( 0, 0, sizeof( leap_t ) );
        if ( NULL == new_table->leap_seconds )
        {
            free_leaptable( new_table );
            return NULL;
        }

        // add the 10 leap seconds that already existed between UTC and TAI onJan
        leap_t utc_start_leap;
        utc_start_leap.transition = NTP_TIME_JAN1( 1972 );
        int total_change = 10;
        utc_start_leap.change = total_change;

        g_array_append_val( new_table->leap_seconds, utc_start_leap );

        while ( !feof( f ) )
        {
            fgets( line, sizeof( line ), f );
            if ( line[ 0 ] == '#' )
            {
                if ( line[ 1 ] == 'E' )
                {
                    // expiration time is seconds from UNIX epoch
                    uint64_t expire_unix = 0;
                    int      num_matched =
                        sscanf( line, "#Expires %lu", &expire_unix );
                    ntp_s_t expire_ntp = expire_unix + UNIX_EPOCH;

                    if ( num_matched == 0 )
                    {
                        fprintf( stderr,
                                 "failed to get expiration time from %s\n",
                                 fname );
                        expire_ntp = 0;
                    }
                    new_table->expiration = expire_ntp;
                }
            }
            if ( strncmp( line, "Leap", 4 ) == 0 )
            {
                leap_t   new_leap;
                utc_t    new_utc;
                char     monthname[ 16 ];
                uint64_t year;
                char     change_symbol[ 16 ];

                int num_matched = sscanf( line,
                                          "Leap %lu %s %d %d:%d:%d %s",
                                          &year,
                                          monthname,
                                          &new_utc.tm_mday,
                                          &new_utc.tm_hour,
                                          &new_utc.tm_min,
                                          &new_utc.tm_sec,
                                          change_symbol );
                if ( 7 == num_matched )
                {
                    new_utc.tm_year = year - 1900;
                    if ( strcmp( monthname, "Dec" ) == 0 )
                    {
                        new_utc.tm_mon = 11;
                    }
                    else if ( strcmp( monthname, "Jun" ) == 0 )
                    {
                        new_utc.tm_mon = 5;
                    }
                    else
                    {
                        fprintf( stderr,
                                 "Unrecognized month %s in %s.  Only Dec "
                                 "and Jun allowed for leap seconds.\n",
                                 monthname,
                                 fname );
                        continue;
                    }

                    int change_num = 0;
                    if ( strcmp( change_symbol, "+" ) == 0 )
                    {
                        change_num = 1;
                    }
                    else if ( strcmp( change_symbol, "-" ) == 0 )
                    {
                        change_num = -1;
                    }
                    else
                    {
                        fprintf(
                            stderr,
                            "Unrecognized leap second change symbol in %s.",
                            fname );
                        continue;
                    }

                    total_change += change_num;

                    const unsigned short int* days_before =
                        mon_yday[ ISLEAP( year ) ? 1 : 0 ];
                    new_utc.tm_yday =
                        days_before[ new_utc.tm_mon ] + new_utc.tm_mday - 1;

                    new_leap.transition = NTPfromUTC_s( &new_utc );
                    new_leap.change = total_change;

                    g_array_append_val( new_table->leap_seconds, new_leap );
                }
                else
                {
                    fprintf( stderr,
                             "error reading table entry from file %s\n",
                             fname );
                }
            }
        }
        fclose( f );
        return new_table;
    }
    return NULL;
}

/**
 * Load leap seconds into the global pointer atomically
 *
 * This is a thread-safe function.  Since leapseconds only need
 * to be loaded rarely, we don't care about races.
 * return if there's a collision.
 */
static void
load_leap_seconds( )
{
    leap_table_t* new_table;

    if ( NULL == ( new_table = read_IERS_leap_seconds_linux( ) ) )
    {
        if ( NULL == ( new_table = read_NIST_leap_seconds_linux( ) ) )
        {
            if ( NULL == ( new_table = get_fallback_leaps( ) ) )
            {
                fprintf( stderr, "failed to load any leap second data.\n" );
                return;
            }
        }
    }
    leap_table_t* old_table = g_atomic_pointer_get( &leaps );
    if ( ( old_table == NULL ) ||
         ( new_table->expiration > old_table->expiration ) )
    {
        if ( !g_atomic_pointer_compare_and_exchange(
                 &leaps, old_table, new_table ) )
        {
            /* someone else already set the table */
            free_leaptable( new_table );
        }
    }
    else
    {
        /* table isn't new, drop it */
        free_leaptable( new_table );
        /* check if the current table is out of date */
        ntp_s_t now = time( NULL ) + UNIX_EPOCH;
        if ( now > old_table->expiration )
        {
            fprintf( stderr, "WARNING: leap seconds table has expired\n" );
        }
    }
}

/**
 * Returns the current leap second table.
 * Will try to load if there is none.
 *
 * If it's been 24 hours or more since the last load, will also start
 * an async load.
 * @return
 */
static leap_table_t*
get_leap_second_table( )
{
    static time_t last_load = 0;
    leap_table_t* table = g_atomic_pointer_get( &leaps );
    if ( NULL == table )
    {
        load_leap_seconds( );
        last_load = time( NULL );
        table = g_atomic_pointer_get( &leaps );
    }
    return table;
}

leap_t zero_leap = { 0, 0 };

/**
 * Return number of added leapseconds accrued to the given NTP.
 * Can theoretically be negative.
 * @param ntp_s
 * @return 0 if there is no leap table or for dates before Jan 1, 1972
 */
static leap_t*
leap_seconds_from_ntp( ntp_s_t ntp_s )
{
    leap_table_t* table = get_leap_second_table( );
    if ( table == NULL || table->leap_seconds->len < 1 )
    {
        return 0;
    }
    leap_t* leap_seconds = (leap_t*)table->leap_seconds->data;
    int     low = 0;
    int     high = table->leap_seconds->len - 1;

    /** check high value first.  This will be the most common result.
     * older times can be a tiny bit slower.
     */
    if ( ntp_s >= leap_seconds[ high ].transition )
    {
        return leap_seconds + high;
    }
    while ( high - low > 1 )
    {
        int mid = ( high + low ) / 2;
        if ( ntp_s >= leap_seconds[ mid ].transition )
        {
            low = mid;
        }
        else
        {
            high = mid;
        }
    }
    if ( low == 0 && ntp_s < leap_seconds[ low ].transition )
    {
        return &zero_leap;
    }
    return leap_seconds + low;
}

static gps_s_t
GPSfromTAI_s( tai_s_t t )
{
    return t - TAIatGPSzero;
}

static tai_s_t
TAIfromGPS_s( gps_s_t t )
{
    return t + TAIatGPSzero;
}

static gps_t*
GPSfromTAI( tai_t* tai, gps_t* gps )
{
    if ( ( NULL != tai ) && ( NULL != gps ) )
    {
        gps->nanoseconds = tai->nanoseconds;
        gps->seconds = GPSfromTAI_s( tai->seconds );
        return gps;
    }
    return NULL;
}

static tai_t*
TAIfromGPS( gps_t* gps, tai_t* tai )
{
    if ( ( NULL != tai ) && ( NULL != gps ) )
    {
        tai->nanoseconds = gps->nanoseconds;
        tai->seconds = TAIfromGPS_s( gps->seconds );
        return tai;
    }
    return NULL;
}

/**
 * Convert NTP seconds to TAI seconds
 * @param ntp_s
 * @return
 */
static tai_s_t
TAIfromNTP_s( ntp_s_t ntp_s )
{
    leap_t* leap = leap_seconds_from_ntp( ntp_s );
    return ( ntp_s - TAI_EPOCH + leap->change );
}

static ntp_s_t
NTPfromTAI_s( tai_s_t tai_s )
{
    ntp_s_t guess = tai_s + TAI_EPOCH;

    /* handle the case where subtracting leap seconds drops a leap second */
    int     drop_leap = 0;
    leap_t* leap = leap_seconds_from_ntp( guess );
    if ( guess - leap->change < leap->transition - 1 )
    {
        drop_leap = 1;
    }

    return ( tai_s + TAI_EPOCH - leap->change + drop_leap );
}

/**
 * Fill in a UTC structure from an ntp seconds value
 *
 * Only need seconds resolution because that's all the UTC structure supports
 * @param ntp_s
 * @param utc_ptr
 * @return
 */
static utc_t*
UTCfromNTP( ntp_s_t ntp_s, utc_t* utc_ptr )
{
    if ( NULL == utc_ptr )
    {
        return NULL;
    }
    /* first separate days from the rest */
    int days = ntp_s / SECS_PER_DAY;
    int rem_s = ntp_s % SECS_PER_DAY;
    while ( rem_s < 0 )
    {
        rem_s += SECS_PER_DAY;
        days--;
    }
    while ( rem_s >= SECS_PER_DAY )
    {
        rem_s -= SECS_PER_DAY;
        days++;
    }

    /* fill in hours, minutes, sec */
    utc_ptr->tm_hour = rem_s / SECS_PER_HOUR;
    rem_s %= SECS_PER_HOUR;
    utc_ptr->tm_min = rem_s / 60;
    utc_ptr->tm_sec = rem_s % 60;

    /* fill in week day */
    utc_ptr->tm_wday = ( NTP_EPOCH_WEEKDAY + days ) % 7;
    if ( utc_ptr->tm_wday < 0 )
    {
        utc_ptr->tm_wday += 7;
    }

    /* calculate year */
    int y = NTP_EPOCH_YEAR;
    while ( ( days < 0 ) || ( days >= ( ISLEAP( y ) ? 366 : 365 ) ) )
    {
        /* Guess a corrected year, assuming 365 days per year.  */
        long int yg = y + days / 365 - ( days % 365 < 0 );

        /* Adjust DAYS and Y to match the guessed year.  */
        days -= ( ( yg - y ) * 365 + LEAPS_UNTIL( yg ) - LEAPS_UNTIL( y ) );
        y = yg;
    }

    /* fill in year, days of year */
    utc_ptr->tm_year = y - 1900;
    utc_ptr->tm_yday = days;

    /* fill in month and day of month */
    const uint16_t* ip = mon_yday[ ISLEAP( y ) ];
    for ( y = 11; days < ip[ y ]; --y )
    {
        continue;
    }
    days -= ip[ y ];
    utc_ptr->tm_mon = y;
    utc_ptr->tm_mday = days + 1;
    return utc_ptr;
}

/**
 * Converts tai seconds into a utc.
 * A pointer to a pre-allocated utc_t structure must be passed
 * Since utc only has seconds resolution, this operates only on a TAI second.
 * @param utc_ptr
 * @return
 */
static utc_t*
UTCfromTAI( tai_s_t tai_s, utc_t* utc_ptr )
{
    if ( utc_ptr == NULL )
    {
        return NULL;
    }

    ntp_s_t ntp_s = NTPfromTAI_s( tai_s );

    /* calculate utc from TAI */
    return UTCfromNTP( ntp_s, utc_ptr );
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/*                                                         		*/
/* Procedure Description: converts GPS to UTC				*/
/*----------------------------------------------------------------------*/
utc_t*
UTCfromGPS_s( gps_s_t gps, utc_t* utc_ptr )
{
    tai_s_t tai_s = TAIfromGPS_s( gps );

    return UTCfromTAI( tai_s, utc_ptr );
}

/**
 * Converts nanoseconds since the GPS epoch to a utc structure.
 * Since utc_t is second resolution, fractional parts of the second is lost.
 * @param gps_ns
 * @param utc_ptr
 * @return
 */
utc_t*
UTCfromGPS_ns( gps_ns_t gps_ns, utc_t* utc_ptr )
{
    return UTCfromGPS_s( gps_ns / NSECS_PER_SEC, utc_ptr );
}

gps_s_t
GPSfromUTC_s( utc_t const* utc_ptr )
{
    ntp_s_t ntp_s = NTPfromUTC_s( utc_ptr );
    tai_s_t tai_s = TAIfromNTP_s( ntp_s );
    return GPSfromTAI_s( tai_s );
}

gps_ns_t
GPSfromUTC_ns( const utc_t* utc_ptr )
{
    return GPSfromUTC_s( utc_ptr ) * NSECS_PER_SEC;
}

gps_ns_t
GPS_as_nsec( const gps_t* gps )
{
    if ( NULL == gps )
    {
        return 0;
    }
    return gps->seconds * NSECS_PER_SEC + gps->nanoseconds;
}

gps_pip_t
GPS_as_pip( const gps_t* gps )
{
    if ( NULL == gps )
    {
        return 0;
    }
    uint64_t pip = gps->nanoseconds;
    pip *= NSECS_PER_SEC;
    // rounding
    pip += 1 << 29;
    pip >>= 30;
    return ( gps->seconds << 30 ) + pip;
}

gps_s_t
GPS_from_nsec( gps_ns_t gps_ns, gps_t* gps )
{
    gps_s_t seconds = gps_ns / NSECS_PER_SEC;
    if ( NULL != gps )
    {
        gps->seconds = seconds;
        gps->nanoseconds = gps_ns % NSECS_PER_SEC;
    }
    return seconds;
}

gps_t*
GPSnow( gps_t* gps )
{

    if ( NULL == gps )
    {
        return NULL;
    }
    struct timespec now;
    if ( clock_gettime( CLOCK_REALTIME, &now ) != 0 )
    {
        return NULL;
    }

    ntp_s_t ntp_s = now.tv_sec + UNIX_EPOCH;
    tai_s_t tai_s = TAIfromNTP_s( ntp_s );
    gps->seconds = GPSfromTAI_s( tai_s );
    gps->nanoseconds = now.tv_nsec;
    return gps;
}

gps_ns_t
GPSnow_ns( void )
{
    gps_t gps;
    if ( NULL == GPSnow( &gps ) )
    {
        return 0;
    }
    return GPS_as_nsec( &gps );
}

gps_pip_t
GPSnow_pip( void )
{
    gps_t gps;
    if ( NULL == GPSnow( &gps ) )
    {
        return 0;
    }
    return GPS_as_pip( &gps );
}

gps_s_t
GPSnow_s( void )
{
    gps_t gps;
    if ( NULL == GPSnow( &gps ) )
    {
        return 0;
    }
    return gps.seconds;
}

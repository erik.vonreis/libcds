/* Version: $Id$ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: tconv							*/
/*                                                         		*/
/* Module Description: Time Conversion API	 			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.5	 10Apr98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: tconv.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.5.1		*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _UTC_TAI_H
#define _UTC_TAI_H
#ifndef __CINT__

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */

#include <stdint.h>
#include <time.h>

/** @name Time Conversion API
    * The time conversion API provides functions to convert between
    TAI (international atomic time) and UTC (coordinated universal time).
    It uses an internal table to account for leap seconds. When additional
    leap seconds are announced, the tabel has to be update and the 
    module recompiled.
    This module also provides utility routines to break down a TAI
    format, to convert to and from a network portable representation
    and to access the leap second information.

    Knowm limitations: The conversion only works for dates after
    Jan. 1, 1972.
   
    @memo Converts between UTC and TAI
    @author Written April 1998 by Daniel Sigg
    @version 0.5
************************************************************************/

/*@{*/

/** @name Constants and flags.
    * Constants and flags of the time conversion API.

    @memo Constants and flags
    @author DS, April 98
    @see Time Conversion
************************************************************************/

/*@{*/

/** Defines the TAI offset relative to GPS time. GPS time is expressed in
    weeks and seconds starting from zero at Sun, Jan. 6, 1980, 00:00 UTC.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
#define TAIatGPSzero 694656019UL

/** One second expressed in ns.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
#define _ONESEC_NS 1000000000LL
/*#define _ONESEC     ((tainsec_t)1000000000)*/

/** One hour expressed in ns.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
#define _ONEHOUR_NS ( 3600 * _ONESEC )

/** One day expressed in ns.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
#define _ONEDAY_NS ( 24 * _ONEHOUR )

/*@}*/

/** @name Data types.
    * Data types of the time conversion API.

    @memo Data types
    @author DS, April 98
    @see Time Conversion
************************************************************************/

/*@{*/

#include <inttypes.h>

/** Denotes a type representing TAI in sec.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
typedef int64_t tai_s_t;
typedef int64_t tai_ns_t;

typedef struct tai_t
{
    tai_s_t  seconds;
    tai_ns_t nanoseconds;
} tai_t;

/**
    * Nanoseconds since the gps epoch.
    */
typedef int64_t gps_ns_t;

/**
    * Seconds since the gps epoch.
    */
typedef int64_t gps_s_t;

/**
 * Units, "pips", are 2**-30 seconds.
 * This is slightly less than a nanosecond
 * but is useful being an exact power of two.
 * Representation exact in double precision and
 * it'll represent LIGO channel rates exactly.
 */
typedef int64_t gps_pip_t;

/** Denotes a struct representing TAI broken down in sec and nsec.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
typedef struct gps_t
{
    /** sec part of TAI. */
    gps_s_t seconds;
    /** nsec part of TAI. */
    uint32_t nanoseconds;
} gps_t;

/** Denotes a type representing UTC. It is identical to the struct tm
    defined in <time.h>.

    @author DS, April 98
    @see Time Conversion
************************************************************************/
typedef struct tm utc_t;

/*@}*/

/** @name Functions.
    * Functions of the time conversion API.

    @memo Functions
    @author DS, April 98
    @see Time Conversion
************************************************************************/

/*@{*/

/** Converts GPS time to UTC (coordinated universal
    time). GPS time is defined as Jan. 6, 1980, 00:00. UTC is defined
    to coincide with GPS on Jan. 6, 1980, 00:00. To keep the earth
    period synchronized with UTC, leap seconds are added periodically.
    This function corrects for leap seconds as long as the internal table
    is up to date.

    @param t gps time in sec
    @param utc_ptr pointer to a time/data structure which will return UTC
    @return pointer to UTC structure, NULL if failed
    @author DS, April 98
    @see Time Conversion
************************************************************************/
utc_t* UTCfromGPS_s( gps_s_t t, utc_t* utc_ptr );

/** Converts GPS time to UTC (coordinated universal
    time). GPS zero is defined as Jan. 6, 1980, 00:00. UTC is defined
    to coincide with GPS on Jan. 6, 1989, 00:00. To keep the earth
    period synchronized with UTC, leap seconds are added periodically.
    This function corrects for leap seconds as long as the internal table
    is up to date.

    @param t gps time in nsec
    @param utc_ptr pointer to a time/data structure which will return UTC
    @return pointer to UTC structure, NULL if failed
    @author DS, April 98
    @see Time Conversion
************************************************************************/
utc_t* UTCfromGPS_ns( gps_ns_t t, utc_t* utc_ptr );

/** Converts UTC (coordinated universal time) to GPS time.
 *  GPS zero is defined as Jan. 6, 1980, 00:00. UTC is defined
    as coinciding with GPS at that time. To keep the earth
    period synchronized with UTC, leap seconds are added periodically.
    This function corrects for leap seconds as long as the internal table
    is up to date.

    @param utc_ptr pointer to a time/data structure containing UTC
    @return gps time in sec, 0 if failed
    @author DS, April 98
    @see Time Conversion
************************************************************************/
gps_s_t GPSfromUTC_s( const utc_t* utc_ptr );

/** Converts UTC (coordinated universal time) to GPS time.
 *  TAI zero is defined as Jan. 6, 1980, 00:00. UTC is defined
    to coincide with GPS at that time. To keep the earth
    period synchronized with UTC, leap seconds are added periodically.
    This function corrects for leap seconds as long as the internal table
    is up to date.

    @param utc_ptr pointer to a time/data structure containing UTC
    @return GPS time in nsec, 0 if failed
    @author DS, April 98
    @see Time Conversion
************************************************************************/
gps_ns_t GPSfromUTC_ns( const utc_t* utc_ptr );

/** Returns the current GPS time in nsec. Implementation dependent;
    uses <time.h> on some systems.

    @return current GPS time in nsec
    @author DS, April 98
    @see Time Conversion
************************************************************************/
gps_ns_t GPSnow_ns( void );

/** Returns the current GPS time in pips (2^-30 sec). Implementation dependent;
    uses <time.h> on some systems.

    @return current GPS time in pip
    @author EvR
    @see Time Conversion
************************************************************************/
gps_ns_t GPSnow_pip( void );

/**
    * Returns the current GPS time as seconds and nano-seconds in the t
    * structure.
    *
    * This has a longer range than GPSnow_ns, which is only good through the year 2563.
    * @return
    */
gps_t* GPSnow( gps_t* t );

/** Converts GPS time from a broken down format into one with nsec units.

    @param t broken down GPS
    @return GPS time in nsec
    @author DS, April 98
    @see Time Conversion
************************************************************************/
gps_ns_t GPS_as_nsec( const gps_t* t );

/** Converts GPS from a format with nsec units into one with sec units.
    Also calculates the broken down format if a pointer to a data 
    structure is provided.

    @param t GPS time in nsec
    @param gps pointer to a broken down TAI data structure
    @return GPS time in sec
    @author DS, April 98
    @see Time Conversion
************************************************************************/
gps_s_t GPS_from_nsec( gps_ns_t t, gps_t* gps );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*__CINT__ */
#endif /*_UTC_TAI_H */
